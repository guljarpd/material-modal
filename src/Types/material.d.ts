export interface Color {
    name: string;
    code: string;
}

export interface MaterialInterface {
    id: string;
    name: string;
    color: Color[];
    image: string;
    discription: string;
    availability: number;
}
