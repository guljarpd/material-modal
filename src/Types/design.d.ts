export interface DesignInterface {
   name: string;
   id: string;
   image: string;
}
