export interface FactoryInterface {
    name: string;
    id: string;
}
