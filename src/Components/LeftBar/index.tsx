import React from 'react';
import styled from "styled-components";
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
//
const StyledDiv = styled.div`
width: 268px;
background: #FFFFFF;
`;

//
function Left() {

  return(
    <Drawer variant="permanent">
      <List>
        <ListItem button key={1}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
        </ListItem>
        <ListItem button key={2}>
          <ListItemIcon>
            <MailIcon />
          </ListItemIcon>
        </ListItem>
      </List>
    </Drawer>
  )
}

export default Left;
