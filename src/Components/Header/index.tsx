import React from 'react';
import styled from "styled-components";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
//
const StyledAppBar = styled(AppBar)`
  width: calc(100% - 56px) !important;
  margin-left: 72px !important;
  background-color: #fff !important;
`;

//
function Header() {

  return (
    <StyledAppBar variant="outlined">
      <Toolbar>
        <Typography variant="h6">{'<LF>'}</Typography>
      </Toolbar>
    </StyledAppBar>
  )
}

//
export default Header;
