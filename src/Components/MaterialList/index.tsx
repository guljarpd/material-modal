import React, {useCallback} from 'react';
import styled from "styled-components";
import Grid from '@material-ui/core/Grid';
import Card from "./card";
import ModalDialog from "../Modal";
import { MaterialInterface } from "../../Types/material";

const StyledGridCard = styled(Grid)`
  padding: 10px !important;
  width: 100% !important;
  margin: 0 !important;
`;


const materialDataList = [
   {
      "id":"CT-876",
      "name":"80% Cotton Navy/White Colour Oxford Chambery",
      "color":[
         {
            "name":"INDIANRED",
            "code":"#CD5C5C"
         },
         {
            "name":"LIGHTCORAL",
            "code":"#F08080"
         },
         {
            "name":"SALMON",
            "code":"#FA8072"
         },
         {
            "name":"DARKSALMON",
            "code":"#E9967A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         }
      ],
      "image":"https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
      "discription":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "availability": 1234
   },
   {
      "id":"CT-877",
      "name":"100% Cotton Navy/White Colour",
      "color":[
         {
            "name":"INDIANRED",
            "code":"#CD5C5C"
         },
         {
            "name":"LIGHTCORAL",
            "code":"#F08080"
         },
         {
            "name":"SALMON",
            "code":"#FA8072"
         },
         {
            "name":"DARKSALMON",
            "code":"#E9967A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         }
      ],
      "image":"https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
      "discription":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "availability": 678
   },
   {
      "id":"CT-876",
      "name":"80% Cotton Navy/White Colour Oxford Chambery",
      "color":[
         {
            "name":"INDIANRED",
            "code":"#CD5C5C"
         },
         {
            "name":"LIGHTCORAL",
            "code":"#F08080"
         },
         {
            "name":"SALMON",
            "code":"#FA8072"
         },
         {
            "name":"DARKSALMON",
            "code":"#E9967A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         }
      ],
      "image":"https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
      "discription":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "availability": 1235
   },
   {
      "id":"CT-877",
      "name":"100% Cotton Navy/White Colour",
      "color":[
         {
            "name":"INDIANRED",
            "code":"#CD5C5C"
         },
         {
            "name":"LIGHTCORAL",
            "code":"#F08080"
         },
         {
            "name":"SALMON",
            "code":"#FA8072"
         },
         {
            "name":"DARKSALMON",
            "code":"#E9967A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         },
         {
            "name":"LIGHTSALMON",
            "code":"#FFA07A"
         }
      ],
      "image":"https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
      "discription":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "availability": 2000
   }
]

//
interface ModelI {
  name?: string;
  id?: string;
  image?: string;
  openModel: boolean;
  availability?:number;
}

//
function MaterialList() {

  const [getModelVal, setModelVal] = React.useState<ModelI>({
      name: '',
      id: '',
      image: '',
      openModel: false,
      availability: 0
  });
  //
  // const handleCardCallback = useCallback((name:string, id:string, image:string) => {
  //     setModelVal({
  //       name,
  //       id,
  //       image,
  //       openModel: true
  //     })
  // }, [name, id, image]);

  const handleCardCallback = (name:string, id:string, image:string, availability:number) => {
    console.log('handleCardCallback', name, id, image, availability);
    setModelVal({
      name: name,
      id: id,
      image: image,
      openModel: true,
      availability: availability
    })
  }


  return(
    <React.Fragment>
      <ModalDialog
        name= {getModelVal.name}
        id= {getModelVal.id}
        image= {getModelVal.image}
        openModel= {getModelVal.openModel}
        availability = {getModelVal.availability}
      />
      <Grid container spacing={1}>
        {
          materialDataList.map((listItem, i) => {
            return(
              <StyledGridCard container item xs={12} sm={3} spacing={3} key={i}>
                <Card
                  id= {listItem.id}
                  name= {listItem.name}
                  color= {listItem.color}
                  image= {listItem.image}
                  discription= {listItem.discription}
                  availability= {listItem.availability}
                  onClickCallback = {handleCardCallback}
                />
              </StyledGridCard>
            )
          })
        }
      </Grid>
    </React.Fragment>
  )
}

export default MaterialList;
