import React from 'react';
import styled from "styled-components";
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
// import { MaterialInterface } from "../../Types/material";

//
const StyledCardRoot = styled(Paper)`
  height: 300px;
  /*width: 246px;
  left: 296px;
  top: 20px;*/
  padding: 20px;
  cursor: pointer;
  &:hover {
    background: #FFFFFF;
    box-shadow: 0px 0px 4px rgba(0, 1, 38, 0.25);
    border-radius: 8px;
  }
`;

const StyledImg = styled.img`
  height: 250px;
  width: 100% !important;
  /*width: 214px;
  left: 16px;
  top: 16px;*/
  border-radius: 0px;
  ${StyledCardRoot}:hover & {
    height: 195px;
  }
`;

const StyledTextDiv = styled.div`
  padding: 5px 0;
`;
//
const StyledCardTitle = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 14px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 20px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
  color: #000126 !important;
`;
//
const StyledGreText = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 12px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 16px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
  color: #676773 !important;
`;
const StyledGreTextId = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 12px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 16px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
  color: #676773 !important;
  display: none;
  ${StyledCardRoot}:hover & {
    display: inline;
  }
`;
//
const StyledGreTextMoreColor = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 12px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 16px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
  color: #676773 !important;
  ${StyledCardRoot}:hover & {
    display: none;
  }
`;
//
const StyledGreTextAvailable = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 12px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 16px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
  color: #676773 !important;
  display: none;
  ${StyledCardRoot}:hover & {
    display: inline;
  }
`;

//
const StyledColorRoot = styled.div`
  position: relative;
  display: none;
  ${StyledCardRoot}:hover & {
    display: flex;
  }
`;
//
const StyledShowColor = styled.span`
  height: 20px;
  width: 20px;
  border-radius: 15px;
  margin-right: 10px;
  background: ${props => props.color}
`;

// background: #FFFFFF;
// box-shadow: 0px 0px 4px rgba(0, 1, 38, 0.25);
// border-radius: 8px;

//
interface CallbackInterface {
  name:string;
  id:string;
  image:string;
}

//
interface Color {
    name: string;
    code: string;
}
//
interface MaterialInterface {
    id: string;
    name: string;
    color: Color[];
    image: string;
    discription: string;
    availability: number;
    onClickCallback: (name:string, id:string, image:string, availability:number) => void;
}




function Card(props: MaterialInterface) {

  const handleCardClick = (name:string, id:string, image:string, availability:number) => {
    console.log('selectedItem', name, id, image, availability);

    props.onClickCallback(name, id, image, availability);
    // name, id, image
  }

  return(
    <React.Fragment>
      <StyledCardRoot
        elevation={0}
        onClick = {()=> handleCardClick(props.name, props.id, props.image, props.availability)}
      >
        <StyledImg
          src={props.image}
          alt={props.name}
        />
          <StyledGreTextId>
            {props.id}
          </StyledGreTextId>
          <StyledCardTitle>
            {props.name}
          </StyledCardTitle>
          <StyledGreTextMoreColor>
            {(props.color.length > 1 ? props.color.length + 'more colors': null )}
          </StyledGreTextMoreColor>
          <StyledGreTextAvailable>
            {'Also available in'}
          </StyledGreTextAvailable>
          <StyledColorRoot>
            {props.color.map((item, i) => {
              // display only 6 item
              if(i <= 5){
                  return (<StyledShowColor color={item.code} key={i} />);
              }
            })}
            {(props.color.length > 6 ? '+' + (props.color.length-6): null )}
          </StyledColorRoot>
      </StyledCardRoot>
    </React.Fragment>
  )
}

export default Card;
