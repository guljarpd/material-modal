/* eslint-disable no-use-before-define */
import React from 'react';
import styled from "styled-components";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
//
//
const StyledAutocomplete = styled(Autocomplete)`
  width: 100% !important;
`;
//
const StyledTextField = styled(TextField)`
  & div{
    border-radius: 3px !important;
    font-family: Nunito Sans !important;
    & input {
      padding: 3px !important;
    }
  }
`;
//
const Label = styled('label')`
  padding: 0 0 4px;
  line-height: 1.5;
  display: block;
`;
//
const StyledGrid = styled(Grid)`
  padding-left: 10px;
  font-family: Nunito Sans !important;
`;


//

interface SelectItemI{
  name: string;
  id: string;
  image?: string;
}


interface Data {
  placeholder: string;
  label: string;
  selectItem: SelectItemI[];
  onSelectCallback: (name:string, id:string) => void;
}


//
function AutocompleteComponent(data: Data) {

  console.log('placeholder', data);


  return (
    <React.Fragment>
      <Label>{data.label}</Label>
      <Autocomplete
        id="autocomplete-demo"
        options={data.selectItem}
        getOptionLabel={(option) => option.name}
        onChange={(event: any, selectedValue: SelectItemI | null) => {
          if (selectedValue) {
              data.onSelectCallback(selectedValue.name, selectedValue.id);
          }
        }}
        style={{ width: '100%' }}
        renderInput={(params) =>
          <StyledTextField
            {...params}
            placeholder={data.placeholder}
            variant="outlined"
          />
        }
        renderOption={(option) => {
          console.log('option', option);


          return (
            <Grid container alignItems="center">
              {
                (!option.image)? null :
                <Grid item>
                  <Avatar src={option.image} />
                </Grid>
              }
              <StyledGrid item xs>
                <span style={{ fontWeight: 400 }}>
                  {option.name}
                </span>
                <Typography variant="body2" color="textSecondary">
                  {option.id}
                </Typography>
              </StyledGrid>
            </Grid>
          );
        }}
      />
    </React.Fragment>
  );
}

//
export default AutocompleteComponent;
