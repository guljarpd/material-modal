import React, { useEffect } from 'react';
import styled from "styled-components";
import Header from "../Header";
import LeftBar from "../LeftBar";
import MaterialList from "../MaterialList";

const StyledRootDiv = styled.div`
  overflow: hidden;
`;
const StyledMainContent = styled.main`
  overflow: hidden;
  margin-top: 100px;
  margin-left: 110px;
`;

function Dashboard() {

  return(
    <React.Fragment>
      <Header />
      <LeftBar />
      <StyledMainContent>
        <MaterialList />
      </StyledMainContent>
    </React.Fragment>
  )
}

export default Dashboard;
