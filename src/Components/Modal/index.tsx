import React, {useEffect} from 'react';
import styled from "styled-components";
import Button from '@material-ui/core/Button';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

//
import Autocomplete from "../Autocomplete";
//
import { ReactComponent as BackArrow }  from '../../Assets/icons/BackArrow.svg';
import { ReactComponent as FileIcon }  from '../../Assets/icons/FileIcon.svg';
import { ReactComponent as EyeOpen }  from '../../Assets/icons/EyeOpen.svg';
//
//
function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
//
const StyledDialogTitle = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 20px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 28px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
`;

const StyledCloseCrossButton = styled(IconButton)`
  position: absolute !important;
  right: 8px;
  top: 8px;
  background: #333340;
`;

//
const StyledImg = styled.img`
  /*height: 250px;*/
  width: 100% !important;
  border-radius: 0px;
`;
//
const StyledGridImg = styled(Grid)`
  width: 100% !important;
  margin: 0 !important;
  padding-right: 30px !important;
`;
//
const StyledGridContent = styled(Grid)`
  width: 100% !important;
  margin: 0 !important;
  display: block !important;
`;

//
const StyledContentTitle = styled(Typography)`
  font-family: Nunito Sans !important;
  font-size: 20px !important;
  font-style: normal !important;
  font-weight: 600 !important;
  line-height: 16px !important;
  letter-spacing: 0px !important;
  text-align: left !important;
  color: #000126 !important;
`;

const StyledBackArrow = styled(BackArrow)`
  margin-right: 10px;
`;
//
const StyledTextDiv = styled.div`
  padding: 10px 0;
`;
//
const Label = styled.label`
  padding: 0 0 4px;
  line-height: 1.5;
  display: block;
`;

//
const StyledTextField = styled(TextField)`
  width: 100% !important;
  & div{
    font-family: Nunito Sans !important;
    & input{
      padding: 12px 14px !important;
    }
  }
`;
//
const StyledDivAvailableInventory = styled.div`
  font-family: Nunito Sans !important;
  padding: 10px 0;
`;
//
const StyledDivUploadFile = styled.div`
  font-family: Nunito Sans !important;
  padding: 10px 14px;
  border-style: dashed;
  border-width: 1px;
  border-color: rgba(0, 0, 0, 0.23);
  border-radius: 2px;
`;

//
const StyledFileIcon = styled(FileIcon)`
  float: right;
`;
const StyledEyeOpen = styled(EyeOpen)`
  float: right;
`;

//
const StyledDialogActions = styled(DialogActions)`
  padding: 10px 24px !important;
`;

// back button
const StyledBackButton = styled(Button)`
  border: 1px solid #D4D4D4 !important;
  box-sizing: border-box !important;
  border-radius: 4px !important;
  color: #000126 !important;
  font-family: Nunito Sans !important;
  font-style: normal !important;
  font-weight: bold !important;
  font-size: 14px !important;
  line-height: 20px !important;
  &:hover {
    background: #0067E2 !important;
    color: #fff !important;
    border:none !important;
  }
`;
//
const StyledNextButton = styled(Button)`
  border: 1px solid #D4D4D4 !important;
  background: #D4D4D4 !important;
  box-sizing: border-box !important;
  border-radius: 4px !important;
  color: #fff !important;
  font-family: Nunito Sans !important;
  font-style: normal !important;
  font-weight: bold !important;
  font-size: 14px !important;
  line-height: 20px !important;
  &:hover {
    background: #0067E2 !important;
    color: #fff !important;
    border:none !important;
  }
`;
//
const StyledInfoDiv = styled.div`

`;
const StyledFormDiv = styled.div`
  /*display: none;*/
`;

const StyledAlert = styled(Alert)`
  color: #000126 !important;
  background: #FFFEE6 !important;
  border: 1px solid rgba(0, 1, 38, 0.1) !important;
  box-sizing: border-box !important;
  border-radius: 4px !important;
  margin: 15px 0 !important;
  box-shadow: none !important;
`;

const StyledChallanTextDiv = styled.div`
  padding: 10px 14px;
  background: #F2F2F2;
  border-radius: 4px;
  width: 50%;
`;
// const StyledTextDivSimple = styled.div`
//   padding: 10px 0;
// `;




//
const factoryList = [
   {
      "name":"Amaya Creations",
      "id":"FT-12"
   },
   {
      "name":"Soha Creations",
      "id":"FT-13"
   }
]

//
const designList = [
   {
      "name":"Design name 1",
      "id":"DM-08",
      "image":"https://images.unsplash.com/photo-1548549557-dbe9946621da?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"
   },
   {
      "name":"Design name 2",
      "id":"DM-09",
      "image":"https://images.unsplash.com/photo-1504703395950-b89145a5425b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=351&q=80"
   }
]




export interface DialogTitleProps {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitleCustom = ((props: DialogTitleProps) => {
  const { children, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography {...other}>
      <StyledDialogTitle variant="h2">{children}</StyledDialogTitle>
      {onClose ? (
        <StyledCloseCrossButton aria-label="close" onClick={onClose}>
          <CloseIcon />
        </StyledCloseCrossButton>
      ) : null}
    </MuiDialogTitle>
  );
});


//
interface ModelI {
  name?: string;
  id?: string;
  image?: string;
  availability?: number;
  openModel: boolean;
}
//
interface ModelFormI {
  factoryName: string;
  designName: string;
  quantity: number;
  challanFileName: string;
}


//
function ModalDialog(props: ModelI) {

  const [open, setOpen] = React.useState(false);
  const [getModelState, setModelState] = React.useState<string>('');
  const [getError, setError] = React.useState<string>('');
  const [getForm, setForm] = React.useState<ModelFormI>({
    factoryName: '',
    designName: '',
    quantity: 0,
    challanFileName: ''
  })
  //

  // console.log('props', props);

  useEffect(()=>{
    // console.log('props', props);
    setOpen(props.openModel);
  }, [props])

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  //
  const selectFactory = (name:string, id:string) => {
    // console.log('selectFactory', name, id);
    setForm({
      ...getForm,
      factoryName: name
    });
  }
  const selectDesign = (name:string, id:string) => {
    setForm({
      ...getForm,
      designName: name
    })
  }

  //
  const handleQuantity = (event: React.ChangeEvent<HTMLInputElement>) => {
    // console.log('handleQuantity', event.target.value);
    if(!props.availability) return;
    //
    if (parseInt(event.target.value) > props.availability) {
      // set error  Quantity can't be more then available value
      setError("Quantity can't be more then availability");
    } else if (parseInt(event.target.value) < 1) {
      // set error  Quantity can't be less then 1
      setError("Quantity can't be less then 1");
    }else {
      // set value in state.
      setError("");
      setForm({
        ...getForm,
        quantity: parseInt(event.target.value)
      });
    }
  }
  //
  const handleFile = (event: React.ChangeEvent<HTMLInputElement>) =>{
    // console.log('handleFile', event.target.files);
    if (event.target.files) {
       // event.target.files[0].name
       setForm({
         ...getForm,
         challanFileName: event.target.files[0].name
       })
    }
  }
  //
  const handleBack = () => {
    setModelState('FORM');
  }
  const handleNext = () => {
    if (!getForm.factoryName || !getForm.designName || !getForm.quantity || !getForm.challanFileName) return;
    setModelState('DETAILS');
  }


  return (
    <React.Fragment>
      <Dialog
        fullWidth={true}
        maxWidth={'md'}
        open={open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitleCustom id="customized-dialog-title" onClose={handleClose}>
          {'Material details'}
        </DialogTitleCustom>
        <DialogContent>
          <Grid container spacing={1}>
            <StyledGridImg container item xs={4} sm={4} spacing={3}>
              <StyledImg
                src={props.image}
                alt={props.name}
              />
            </StyledGridImg>
            <StyledGridContent container item xs={8} sm={8} spacing={3}>
              <div>
                <StyledContentTitle>
                  <StyledBackArrow /> {'Assign to factory'}
                </StyledContentTitle>
              </div>

              {/* Form Info */}
              <StyledInfoDiv
                style={(getModelState !=='DETAILS')? {display: 'none'} : {display: 'block'}}
              >
                <StyledAlert severity="info">You won’t be able to change the details later.</StyledAlert>
                <StyledTextDiv>
                  <Label style={{color: '#676773'}}>{'Factory'}</Label>
                  <StyledTextDiv style={{padding: '5px 0'}}>
                    {getForm.factoryName}
                  </StyledTextDiv>
                </StyledTextDiv>
                <StyledTextDiv>
                  <Label style={{color: '#676773'}}>{'Assign for design'}</Label>
                  <StyledTextDiv style={{padding: '5px 0'}}>
                    {getForm.designName}
                  </StyledTextDiv>
                </StyledTextDiv>
                <StyledTextDiv>
                  <Label style={{color: '#676773'}}>{'Assign quantity'}</Label>
                  <StyledTextDiv style={{padding: '5px 0'}}>
                    {getForm.quantity} {'meter'}
                  </StyledTextDiv>
                </StyledTextDiv>
                <StyledTextDiv>
                  <Label style={{color: '#676773'}}>{'Challan'}</Label>
                  <StyledChallanTextDiv style={{padding: '5px 0'}}>
                    {getForm.challanFileName}
                    <StyledEyeOpen />
                  </StyledChallanTextDiv>
                </StyledTextDiv>
              </StyledInfoDiv>

              {/* Form*/}
              <StyledFormDiv
                style={(getModelState ==='DETAILS')? {display: 'none'} : {display: 'block'}}
              >
                <StyledTextDiv>
                  <Autocomplete
                    selectItem={factoryList}
                    label={'Factory*'}
                    placeholder={"Select Factory"}
                    onSelectCallback={selectFactory}
                  />
                </StyledTextDiv>
                <StyledTextDiv>
                  <Autocomplete
                    selectItem={designList}
                    label={'Assign for design*'}
                    placeholder={"Select Design"}
                    onSelectCallback={selectDesign}
                  />
                </StyledTextDiv>
                <StyledTextDiv>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Label>{'Assign quantity*'}</Label>
                      <StyledTextField
                        error={(getError !=='')? true : false}
                        id="outlined-margin-dense"
                        placeholder={'Enter quantity'}
                        variant="outlined"
                        type="number"
                        onChange={handleQuantity}
                        helperText= {(getError !=='')? getError : null}

                      />
                    </Grid>
                    {/*   */}
                    <Grid item xs={6}>
                      <Label>{'Available Inventory'}</Label>
                      <StyledDivAvailableInventory>
                        {props.availability} {'meter'}
                      </StyledDivAvailableInventory>
                    </Grid>
                  </Grid>
                </StyledTextDiv>
                <StyledTextDiv>
                  <Grid container spacing={3}>
                    <Grid item xs={6}>
                      <Label>{'Attach Challan*'}</Label>
                      <label htmlFor="file-upload">
                        <input
                          style={{ display: "none" }}
                          id="file-upload"
                          name="file-upload"
                          type="file"
                          onChange={handleFile}
                        />
                        <StyledDivUploadFile>
                          {'Select File'}
                          <StyledFileIcon />
                        </StyledDivUploadFile>
                      </label>

                    </Grid>
                  </Grid>
                </StyledTextDiv>
              </StyledFormDiv>
            </StyledGridContent>
          </Grid>
        </DialogContent>
        <Divider />
        <StyledDialogActions>
          <StyledBackButton
            onClick={handleBack}
            color="primary"
          >
            Back
          </StyledBackButton>
          <StyledNextButton
            onClick={handleNext}
            color="primary"
          >
            Next
          </StyledNextButton>
        </StyledDialogActions>
      </Dialog>
    </React.Fragment>
  );
}

export default ModalDialog;
